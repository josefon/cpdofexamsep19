package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
	@Test
	public void getWhichOneIsBiggerTestSecondParamIsBigger() throws Exception{
		MinMax minMax = new MinMax();
		int number = minMax.getWhichOneIsBigger(1, 2);
		assertEquals("Method MinMax.getWhichOneIsBigger is nor returning correct result: ", 2, number);
	}
	
	@Test
	public void getWhichOneIsBiggerTestFirstParamIsBigger() throws Exception{
		MinMax minMax = new MinMax();
		int number = minMax.getWhichOneIsBigger(3, 2);
		assertEquals("Method MinMax.getWhichOneIsBigger is nor returning correct result: ", 3, number);
	}
}