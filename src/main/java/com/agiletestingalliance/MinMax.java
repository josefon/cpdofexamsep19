package com.agiletestingalliance;

public class MinMax {

    public int getWhichOneIsBigger(int firstNumber, int secondNumber) {
        if (secondNumber > firstNumber) {
            return secondNumber;
        } else {
            return firstNumber;
        }
    }

}
